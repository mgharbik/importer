# CSV uploader

## Background

1. Assume the files have been uploaded by a User.
2. Assume the files originated from the same external program.
3. The files have different types of data
4. Assume at least the following ForecastXL DB models:

* Relation <id, code, name, gender>
* Project <id, code, name, starts_at, ends_at>
* Fact <id, value, date, comment, project_id, relation_id, product_id>

## Goal

The User wants to import the data into ForecastXL. While doing so the User wants a certain level of control over which and how this data is imported.

## Steps

Step 1: Parse a CSV

Step 2: Transform the data into a format which ForecastXL can understand. You do not have to write the data to a database, it will suffice to create arrays with hashes.

[{ column_name: value, column_name: value }, { column_name: value, column_name: value }, ...]

Step 3: Allow the User to provide settings on how to map the data to ForecastXL.
    The User should be able to tell which column should be imported and where it should be stored. You do not have to worry about the interface.

Step 4: Allow the User to provide additional settings:

* The User should be able to provide a default value for blanks.
* The User should be able to to reject a record if the value in a certain column is blank.
* The User should be able to allow only to import records from a certain timeperiod.
